#/bin/bash

# Define the clock
# TempSens() {
#     TEMPC=$(sensors | ack temp w
# }
Clock() {
    DATE=$(date "+%a %b %d, %T")
    echo -n "$DATE"
}
Battery() {
    BAT=$(acpi -V | ack -1 -o ..%)
    echo -n "Battery: $BAT"
}
Volume() {
    VOL=$(amixer sget 'Master' | ack Left | ack -o ..%)
    echo -n "Volume: $VOL"
}

# Print to bar
while true; do
    # echo "%{l}%{Fyellow}%{Bblue} $(Volume) %{c}%{Fyellow}%{Bblue} $(Clock) %{r}%{Fyellow}%{Bblue} $(Battery)%{F-}"
    echo "%{l} $(Volume) %{c} $(Clock) %{r} $(Battery)%{F-}"
    sleep 1;
done

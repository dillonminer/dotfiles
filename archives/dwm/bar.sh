# configure dwm status bar
while true; do

    # Volume Level
    DWM_VOL=$( amixer -c0 sget Master | awk -vORS=' ' '/Mono:/ {print($4)}' );
    # clock
    DWM_CLOCK=$( date '+%e %b %Y %a | %k:%M' );

    # output
    DWM_STATUS="Vol: $DWM_VOL | $DWM_CLOCK";
    xsetroot -name "$DWM_STATUS";
    sleep $
    sleep 1s;
done &

#dotfiles

these are my dotfiles.
I have intentions of tidying them up further and making them more accessible
to more people. For now, it is what it is.

### Goals

##### Vim
Never ending. Working to get rid of heavy plugins and switch to lighter
solutions.

##### WM
In the process of switching to DWM from BSPWM. Running dwm pretty near stock
at the moment. hopefully I will get that shit in order soon.

#### Programs that Need install

##### Programming
neovim
tmux
ctags
java
ag
ack

##### desktop related stuff
dwm
redshift
###### laptop specific
xbacklight
xfce4-power-manager

##### general workflow
zathura

#!/bin/bash

# backup bashrc
mkdir ~/temp_backups
mv ~/.bashrc ~/temp_backups/bashrc

# bash links
ln -s ~/pkg/dotfiles/bashrc ~/.bashrc
ln -s ~/pkg/dotfiles/bash_alias ~/.bash_alias

ln -s ~/pkg/dotfiles/tmux.conf ~/.tmux.conf

mkdir ~/.ncmpcpp
ln -s ~/pkg/dotfiles/ncmpcpp/config ~/.ncmpcpp/config
ln -s ~/pkg/dotfiles/ncmpcpp/bindings ~/.ncmpcpp/bindings
mkdir ~/.bin
ln -s ~/pkg/dotfiles/bin/sync.sh ~/.bin/sync.sh
ln -s ~/pkg/dotfiles/bin/backupfile.sh ~/.bin/backupfile.sh

# xorg
ln -s ~/pkg/dotfiles/xinitrc ~/.xinitrc
rm ~/.Xresources
ln -s ~/pkg/dotfiles/Xresources ~/.Xresources

# ff startpage
mkdir ~/config/firefox
ln -s ~/pkg/dotfiles/firefoxhome.html ~/.config/firefox/firefoxhome.html

echo "dotfiles linked and configured"

# mpd
mkdir ~/.config/mpd
ln -s ~/pkg/dotfiles/mpd/mpd.conf ~/.config/mpd/mpd.conf
cd ~/.config/mpd
touch database log pid state sticker.sql
mkdir playlists

# Based packages
# ag, fzf, mpd, htop.

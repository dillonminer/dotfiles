#!/bin/bash
rsync -a --delete  \
--exclude ".cache" --exclude ".gconf" --exclude ".kde" --exclude ".dbus" --exclude ".local" \
--exclude ".nano" --exclude "Public" --exclude "Templates" \
/home/dillon /bckp/

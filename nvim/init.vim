" .vimrc (designed for nvim but should be vim compatible)
filetype off

" call pathogen to load plugins
execute pathogen#infect()
filetype plugin indent on

" spaces and tabs settings
set tabstop=4
set shiftwidth=4
set textwidth=77
set shiftround " Round indentation to multiple of shiftwidth
set autoindent
set smartindent


" folding
set foldenable
set foldlevelstart=10
set foldnestmax=10
set foldmethod=indent

" search settings
set ignorecase smartcase " case insensetive search unless caps are used

set showmatch " when a bracket is inserted tempoarily highlight matching one

set showcmd " Show commands as i type them

" Enhance command-line completion
set wildmenu
set wildmode=longest,full
set wildignore+=*/.git*
set wildignore+=*.gif,*.png,*.jp*


" general settings
let mapleader=" "
set virtualedit=all
set modeline
set modelines=1

" line numbers
set number

" backup directories
set backupdir=./.backup,.,/tmp
set backupdir=.,./.backup,/tmp

" undofile
set undofile
set undolevels=1000
set undoreload=10000

" use augroup 
au BufWritePre /tmp/* setlocal noundofile
au BufWritePre private/tmp/* setlocal noundofile

" augroup version
" augroup noundofile
" augroup END

" Auto change dir to file directory
set autochdir

" ##########################################
" #    Plugin-specific settings            #
" ##########################################
"
" rainbow-parens
let g:rainbow_active = 1

" javadoc lookup with K
set keywordprg=

" keymaps section
" maps

" Swap ' and ` mark keys
nnoremap ' `
nnoremap ` '

" Change behavior of upercase Y in normal mode
" yank from the cursor to EOL
nnoremap Y y$

" format indentation
nnoremap <Leader>fd mpgg=G`p

" Window Management
" better window switching
nnoremap <c-h> <c-w>h
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-l> <c-w>l

" resize windows with arrow keys
nnoremap <left> <c-w><
nnoremap <right> <c-w>>
nnoremap <up> <c-w>+
nnoremap <down> <c-w>-

" unhighlight previous search with escape
nnoremap <silent> <esc> :nohlsearch<CR><esc>

" fugitive git bindings
nnoremap <leader>ga :Git add %:p<CR><CR>
nnoremap <leader>gs :Gstatus<CR>
nnoremap <leader>gb :Gblame<CR>
nnoremap <leader>gc :Gcommit -v -q<CR>
nnoremap <leader>gd :Gdiff<CR>
nnoremap <leader>gr :Gread<CR>
nnoremap <leader>gw :Gwrite<CR><CR>
nnoremap <leader>gl :silent! Glog<CR>:bot copen<CR>
nnoremap <leader>gp :Ggrep<Space>
nnoremap <leader>gm :Gmove<Space>
" nnoremap <leader>gb :Git branch<Space>
nnoremap <leader>go :Git checkout<Space>
nnoremap <leader>gps :Dispatch! git push<CR>
nnoremap <leader>gpl :Dispatch! git pull<CR>

" toggle tagbar
nnoremap <leader>tt :TagbarToggle<CR>

" edit file in dir
nnoremap <Leader>e :find *

" buffer navigation
nnoremap gb :ls<CR>:buffer<Space>
nnoremap <Leader>b :buffer *
nnoremap <BS> :b#<CR>

" xclip with vim-gnome
nnoremap <leader>p "+p
nnoremap <leader>y "+y

" augroup spaces
"     autocmd!
"     autocmd BufWritePre * %s/\s\+$//e
" augroup END

" terminal
if has('nvim')
    tnoremap jk <C-\><C-n>
    nnoremap <leader>o :below 10sp term://$SHELL<CR>i
endif

" set background=dark
" colorscheme gruvbox
" colorscheme monokai
colorscheme PaperColor
" colorscheme wal

source ~/.config/nvim/statusline.vim " use plugin or runtime

" vim:foldmethod=marker:foldlevel=0

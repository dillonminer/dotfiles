
# !/bin/sh
#
# fzf (actual program for linux)
# git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
# ~/.fzf/install &&

mkdir -p ~/.config/nvim/autoload ~/.config/nvim/bundle &&
curl -LSso ~/.config/nvim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
cd ~/.config/nvim/bundle

# tpope
git clone https://github.com/tpope/vim-unimpaired &
git clone https://github.com/tpope/vim-fugitive &
git clone https://github.com/tpope/vim-commentary &
git clone https://github.com/tpope/vim-sensible &
git clone https://github.com/tpope/vim-surround &
git clone https://github.com/tpope/vim-repeat &
git clone https://github.com/tpope/vim-vinegar &
git clone https://github.com/tpope/vim-rsi &
git clone https://github.com/tpope/vim-eunuch &
git clone https://github.com/tpope/vim-obsession &
git clone https://github.com/tpope/vim-endwise &
git clone https://github.com/tpope/vim-sleuth &
git clone https://github.com/tpope/vim-dispatch &

# color schemes
git clone https://github.com/dylanaraps/wal.vim &
git clone https://github.com/morhetz/gruvbox &
git clone https://github.com/nanotech/jellybeans.vim &
git clone https://github.com/chriskempson/vim-tomorrow-theme &
git clone https://github.com/sickill/vim-monokai &
git clone https://github.com/NLKNguyen/papercolor-theme

# better syntax highlighting
git clone https://github.com/sheerun/vim-polyglot &
# rainbow parens
git clone https://github.com/luochen1990/rainbow &

# dev tools
git clone https://github.com/majutsushi/tagbar &
# java doc
git clone https://github.com/davetron5000/java-javadoc-vim &

# snipmate stuff
git clone https://github.com/MarcWeber/vim-addon-mw-utils &
git clone https://github.com/tomtom/tlib_vim &
git clone https://github.com/honza/vim-snippets &
git clone https://github.com/garbas/vim-snipmate &

# git clone https://github.com/rstacruz/vim-closer
git clone https://github.com/jiangmiao/auto-pairs &
# git clone https://github.com/benekastah/neomake &

# git clone https://github.com/christoomey/vim-tmux-navigator
git clone https://github.com/mhinz/vim-startify &
# git clone https://github.com/bling/vim-airline

# ale asychronous lnit engine
git clone https://github.com/w0rp/ale

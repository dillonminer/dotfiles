set statusline=%f\ -\ Filetype:\%Y
" set statusline=%t  		" filename

set statusline+=%4m  		" path to file relative to current directory

set statusline+=%=  		" right justify from here on

set statusline+=%{fugitive#statusline()}
                                " fugitive#statusline()
set statusline+=Lines:\ %-L\ -\ Col:\ %-c    		" total lines
" :set statusline=%<%f\ %h%m%r%=%-14.(%l,%c%V%)\ %P

" set statusline=%<%f\ %h%m%r%{fugitive#statusline()}%=%-14.(%l,%c%V%)\ %PA

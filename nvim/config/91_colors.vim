" This file seems silly, but I love switching between many different color
" schemes frequently so I made it.
set background=dark

" colorscheme
" colorscheme jellybeans
" colorscheme gruvbox
colorscheme monokai

" Tomorrow (ships with 5 so it gets its own paragraph)
" colorscheme Tomorrow
" colorscheme Tomorrow-Night
" colorscheme Tomorrow-Night-Eighties
" colorscheme Tomorrow-Night-Bright
" colorscheme Tomorrow-Night-Blue

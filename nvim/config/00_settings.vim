" Basic config

"spaces & tabs
set tabstop=4
set shiftwidth=4
set textwidth=77
set shiftround              " Round indentation to a multiple of shiftwidth

" folding
set foldenable
set foldlevelstart=10
set foldnestmax=10
set foldmethod=indent

" search settings
set ignorecase smartcase     " case insensitive search unless caps are used

set showmatch               " Pretty sure this matches parrens

" asthetic
set showcmd                 " Show commands as I type them
set cursorline
":let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1

" general settings
let mapleader=" "
set virtualedit=all
set modeline
set modelines=1             " always read the last line of file

" set relative number and number, the current line shows
" true line number and others are relative
set relativenumber
set number

" backup directories
set backupdir=./.backup,.,/tmp
set backupdir=.,./.backup,/tmp

" mouse settings
set mouse=n

" undofile
set undofile
set undolevels=1000
set undoreload=10000
au BufWritePre /tmp/* setlocal noundofile
au BufWritePre private/tmp/* setlocal noundofile

command! SC vnew | setlocal nobuflisted buftype=nofile bufhidden=wipe noswapfile

" ##########################################
" #    Plugin-specific settings            #
" ##########################################
"
" rainbow-parens
let g:rainbow_active = 1
"
" easy motion
nmap s <Plug>(easymotion-s)
let g:EasyMotion_smartcase = 1

" vim:foldmethod=marker:foldlevel=0

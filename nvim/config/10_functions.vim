" remove trailing whitespace
func! DeleteTrailingWS()
    exe "normal mp"
    %s/\s\+$//ge
    exe "normal 'p"
endfunc

" vim: set et ts=4 sw=4 :

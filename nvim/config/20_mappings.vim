" Swap ' and `
nnoremap ' `
nnoremap ` '
" Change behavior of upercase Y in normal mode
nnoremap Y y$

" use S to move to the first character, carrot is a bitch to reach
nnoremap S ^

" format indentation
nnoremap <Leader>fd mpgg=G`p

" window management
" cycle through windows with tab
" nnoremap <tab> <c-w>w
" nnoremap <s-tab> <c-w>W
" removed becuase I found out that is what is breaking the jumplist
nnoremap <c-h> <c-w>h
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-l> <c-w>l

" create a new vsplit and switch to it
nnoremap <Leader><CR> <C-w>v<C-w>l

" resize windows with arrow keys
nnoremap <left> <c-w><
nnoremap <right> <c-w>>
nnoremap <up> <c-w>+
nnoremap <down> <c-w>-

" undo tree
nnoremap <F3> :UndotreeToggle<CR>

" unhighlight previous search with escape
nnoremap <silent> <esc> :nohlsearch<CR><esc>

" reload vim rc
nnoremap <leader>rv :source ~/.config/nvim/init.vim<CR>
" Neomake
nnoremap <leader>mk :Neomake<CR>

" write file
nnoremap <CR> :w<CR>

" git
nnoremap <leader>gs :Gstatus<CR>
nnoremap <leader>gc :Gcommit
nnoremap <leader>gpl :Gpull<CR>
nnoremap <leader>gpu :Gpush

" edit file in dir
nnoremap <Leader>e :find *

" buffer navigation
nnoremap gb :ls<CR>:buffer<Space>
nnoremap <Leader>b :buffer *
nnoremap <BS> :b#<CR>

" xclip with vim-gnome
nnoremap <leader>p "+p
nnoremap <leader>y "+y

" terminal
if has('nvim')
    :tnoremap jk <C-\><C-n>
    nnoremap <leader>o :below 10sp term://$SHELL<CR>i
endif


" vim:foldmethod=marker:foldlevel=0

#!/bin/bash

# backup bashrc
mkdir ~/temp_backups
mv ~/.bashrc ~/temp_backups/bashrc

# bashrc and bash alias
ln -s ~/src/dotfiles/bash/bashrc ~/.bashrc
# Commented out until alias file is viewed
# ln -s ~/src/dotfiles/bash/bash_alias ~/.bash_alias 

# tmux
ln -s ~/src/dotfiles/tmux.conf ~/.tmux.conf

# xorg
ln -s ~/src/dotfiles/xinitrc ~/.xinitrc
rm ~/.Xresources
ln -s ~/src/dotfiles/Xresources ~/.Xresources


